package publictools;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ToolAggregation {
    public static String subString(String str, String strStart, String strEnd) {
        /* 找出指定的2个字符在 该字符串里面的 位置 */
        int strStartIndex = str.indexOf(strStart);
        int strEndIndex = str.indexOf(strEnd);
        /* index 为负数 即表示该字符串中 没有该字符 */
        if (strStartIndex < 0) {
            return "字符串 :---->" + str + "<---- 中不存在 " + strStart + ", 无法截取目标字符串";
        }
        if (strEndIndex < 0) {
            return "字符串 :---->" + str + "<---- 中不存在 " + strEnd + ", 无法截取目标字符串";
        }
        /* 开始截取 */
        String result = str.substring(strStartIndex, strEndIndex).substring(strStart.length());
        return result;
    }

    public static QueryDataSet connectSql(String sqlurl, String username, String password) throws Exception {
        Connection conn = null;
        QueryDataSet dataSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://192.168.1.247:3307/bpms", "root", "redhat");

            IDatabaseConnection connection = new DatabaseConnection(conn);
            dataSet = new QueryDataSet(connection);
            return dataSet;
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            return dataSet;
        }
    }

    public static String[] readsqlconfig() {
        String[] readsqlconfig = new String[3];
        try {
            File sqlconfig = new File("E:\\working\\EssaTest\\src\\main\\java\\config\\sqlconnect.xml");
            SAXReader reader = new SAXReader();
            Document doc = reader.read(sqlconfig);
            Element root = doc.getRootElement();
            readsqlconfig[0] = root.elementText("sqlurl");
            readsqlconfig[1] = root.elementText("dbuser");
            readsqlconfig[2] = root.elementText("dbpassword");
            return readsqlconfig;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return readsqlconfig;
    }
}


