package bpmsTools;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import publictools.ToolAggregation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SourceBpms {

    public static String[] crawlBpmsItAndJssion(CloseableHttpResponse response) throws IOException {
        String[] crawllogin = new String[2];
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            String entityStr = EntityUtils.toString(entity, "utf-8");
            System.out.println(entityStr);
            crawllogin[0] = ToolAggregation.subString(entityStr, "\"lt\" value=\"", "\" />\n" + "\t\t\t\t<input type=\"hidden\"");
            System.out.println(crawllogin[0]);
            crawllogin[1] = ToolAggregation.subString(entityStr, "jsessionid=", "?service");
            System.out.println(crawllogin[1]);
        }
        return crawllogin;
    }

    public static String crawlCustomerSid(String url, String serviceurl, String username, String password) throws IOException {
        String[] crawllogin;
        HttpClientBuilder builder = HttpClientBuilder.create();
        CloseableHttpClient client = builder.build();
        HttpPost httpPostCatchItAndjsessioni = new HttpPost(url + "/cas/login?service=" + serviceurl + "/cas/hook");
        CloseableHttpResponse responseCatchItAndjsessioni = client.execute(httpPostCatchItAndjsessioni);
        crawllogin = SourceBpms.crawlBpmsItAndJssion(responseCatchItAndjsessioni);
        String It = crawllogin[0];
        String jsessioni = crawllogin[1];

        HttpPost httpPost = new HttpPost(url + "/cas/login?service=" + serviceurl + "/cas/hook");
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);
        params.put("_eventId", "submit");
        params.put("lt", It);
        params.put("submit", "登录");
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
        }
        System.out.println("请求参数：" + nvps.toString());
        httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        httpPost.setHeader("Cookie", "JSESSIONID=" + jsessioni + "; _ga=GA1.2.440190523.1513853121");
        httpPost.setHeader("Upgrade-Insecure-Requests", "1");
        httpPost.setHeader("Referer", url + "/cas/login?service=" + serviceurl + "/cas/hook");
        httpPost.setHeader("Origin", url);

        CloseableHttpResponse response = client.execute(httpPost);

        HttpEntity entity = response.getEntity();
        if (entity != null) {
            String entityStr = EntityUtils.toString(entity, "utf-8");
            System.out.println("输出登录结果：" + entityStr);
        }

        Header header = response.getFirstHeader("Location");
        String location = header.getValue();
        System.out.println("重定向地址为：----->>>>>>>>>>>" + location);

        HttpPost httpPostSend = new HttpPost(location);
        CloseableHttpResponse responseGet = client.execute(httpPostSend);
        HttpEntity entityGet = responseGet.getEntity();
        if (entityGet != null) {
            String entityStrGet = EntityUtils.toString(entityGet, "utf-8");
            System.out.println("重定向输出登录结果：----->>>>>>>>>>>" + entityStrGet);
        }
        Header[] headers = responseGet.getAllHeaders();
        String sid = "";
        for (int i = 1; i < headers.length; i++) {
            sid = headers[i - 1].getName() + ":" + headers[i - 1].getValue();
            System.out.println("返回请求" + sid);
        }
        return sid;
    }
}
