package testSuite;

import bpmsTools.SourceBpms;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import publictools.ToolAggregation;

import java.io.IOException;
import java.util.*;

public class TestCaseDemo {
    CloseableHttpClient client;
    String It = "";
    String jsessioni = "";
    String sid = "";
    boolean resultcode = false;

    /*BPMS登录前需要获取访问页面内的It和jsessioni,这里在做登录测试前做访问页面操作，从返回结果中提出出It和jsessioni
    注意数组方法参数值获取crawllogin以及全局变量定义*/
    @Parameters({"url", "serviceurl"})
    @BeforeTest
    public void getIt(String url, String serviceurl) throws IOException {
        String[] crawllogin;
        HttpClientBuilder builder = HttpClientBuilder.create();
        CloseableHttpClient client = builder.build();
        HttpPost httpPost = new HttpPost(url + "/cas/login?service=" + serviceurl + "/cas/hook");
        CloseableHttpResponse response = client.execute(httpPost);
        crawllogin = SourceBpms.crawlBpmsItAndJssion(response);
        It = crawllogin[0];
        jsessioni = crawllogin[1];
    }

    /*登录重定向需要特殊处理，故需要获取到第一个请求中的重定向地址进行二次post请求*/
    @Parameters({"url", "serviceurl", "username", "password"})
    @Test
    public void loginTestCase(String url, String serviceurl, String username, String password) throws IOException {
        HttpClientBuilder builder = HttpClientBuilder.create();
        CloseableHttpClient client = builder.build();

        HttpPost httpPost = new HttpPost(url + "/cas/login?service=" + serviceurl + "/cas/hook");
        Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);
        params.put("_eventId", "submit");
        params.put("lt", It);
        params.put("submit", "登录");
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                nvps.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }
        }
        System.out.println("请求参数：" + nvps.toString());
        httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        httpPost.setHeader("Cookie", "JSESSIONID=" + jsessioni + "; _ga=GA1.2.440190523.1513853121");
        httpPost.setHeader("Upgrade-Insecure-Requests", "1");
        httpPost.setHeader("Referer", url + "/cas/login?service=" + serviceurl + "/cas/hook");
        httpPost.setHeader("Origin", url);

        CloseableHttpResponse response = client.execute(httpPost);

        HttpEntity entity = response.getEntity();
        if (entity != null) {
            String entityStr = EntityUtils.toString(entity, "utf-8");
            System.out.println("输出登录结果：" + entityStr);
        }

        Header header = response.getFirstHeader("Location");
        String location = header.getValue();
        System.out.println("重定向地址为：----->>>>>>>>>>>" + location);

        HttpPost httpPostSend = new HttpPost(location);
        CloseableHttpResponse responseGet = client.execute(httpPostSend);
        HttpEntity entityGet = responseGet.getEntity();
        if (entityGet != null) {
            String entityStrGet = EntityUtils.toString(entityGet, "utf-8");
            System.out.println("重定向输出登录结果：----->>>>>>>>>>>" + entityStrGet);
        }
        Header[] headers = responseGet.getAllHeaders();
        for (int i = 1; i < headers.length; i++) {
            sid = headers[i - 1].getName() + ":" + headers[i - 1].getValue();
            System.out.println("返回请求" + sid);
        }
    }

    @Parameters({"serviceurl"})
    @Test
    public void queryBuyerTestCase(String serviceurl) throws IOException {
        HttpClientBuilder builder = HttpClientBuilder.create();
        CloseableHttpClient client = builder.build();
        HttpPost httpPostT = new HttpPost(serviceurl + "/data/bpms/buyer/buyer/archives%20");

        String postSid = ToolAggregation.subString(sid, "Set-Cookie:sid=", "; Expires=");
        System.out.println("sid的值为------>>>>>>>" + postSid);

        httpPostT.setHeader("Content-Type", "application/json;charset=UTF-8");
        httpPostT.setHeader("Cookie", "_ga=GA1.2.440190523.1513853121;" + "sid=" + postSid);

        String queryJson = "{\"condition\":{\"code\":\"UKR09160\"},\"pagingQuery\":{\"pageIndex\":1,\"pageSize\":50}}";
        StringEntity requestEntity = new StringEntity(queryJson, "utf-8");
        requestEntity.setContentEncoding("UTF-8");
        requestEntity.setContentType("application/json");
        httpPostT.setEntity(requestEntity);

        CloseableHttpResponse response = client.execute(httpPostT);
        HttpEntity entity = response.getEntity();
        String entityStr = null;
        if (entity != null) {
            entityStr = EntityUtils.toString(entity, "utf-8");
            System.out.println("输出查询结果：" + entityStr);
        }

        //转换返回json格式并获取key：success的值作为后续断言结果
        JSONObject jo = JSONObject.parseObject(entityStr);
        resultcode = jo.getBoolean("success");
        System.out.println("输出查询结果：---------------->>>>>>>>>>>>" + resultcode);

        //加断言，resultcode为作为判断的原值，expcted：false为期望值，第三个参数为说明信息，注意原值和期望值类型
        Assert.assertEquals(resultcode, true, "Not equals:");
    }

    @AfterSuite
    public void closeClient() {
        try {
            if (client != null) {
                client.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
