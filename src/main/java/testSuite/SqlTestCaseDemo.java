package testSuite;

import org.dbunit.Assertion;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import publictools.ToolAggregation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.*;

public class SqlTestCaseDemo {
    public static void main(String[] args) {
        Connection conn = null;
        try {
            //读取配置文件链接数据库
            QueryDataSet dataSet = ToolAggregation.connectSql(ToolAggregation.readsqlconfig()[0],ToolAggregation.readsqlconfig()[1],ToolAggregation.readsqlconfig()[2]);

            //直接在数据库根据采购商编码查询结果，并将查询的结果数据写入result文件
            dataSet.addTable("buy_buyer","select * from buy_buyer where code ='UKR09160'");
            FlatXmlDataSet.write(dataSet, new FileOutputStream("E:\\working\\EssaTest\\src\\main\\java\\xml\\result.xml"));
            IDataSet xmlRsultDataSet = new FlatXmlDataSet(new FileInputStream("E:\\working\\EssaTest\\src\\main\\java\\xml\\result.xml"));
            ITable xmlRsultTable = xmlRsultDataSet.getTable("buy_buyer");
            xmlRsultTable = DefaultColumnFilter.includedColumnsTable(xmlRsultTable, new String[]{"id", "code", "mobile"});

            //读取期望结果配置文件
            IDataSet xmlDataSet = new FlatXmlDataSet(new FileInputStream("E:\\working\\EssaTest\\src\\main\\java\\xml\\assertEquals.xml"));
            ITable xmlTable = xmlDataSet.getTable("buy_buyer");
            xmlTable = DefaultColumnFilter.includedColumnsTable(xmlTable, new String[]{"id", "code", "mobile"});

            //只对比id、code、mobile的值
            Assertion.assertEquals(xmlRsultTable, xmlTable);

        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }


}

