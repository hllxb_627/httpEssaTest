package testSuite;

import bpmsTools.SourceBpms;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.dbunit.Assertion;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import publictools.ToolAggregation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class QueryBuyerTestCase {
    CloseableHttpClient client;
    String sid = "";
    boolean resultcode = false;

    @Parameters({"url", "serviceurl", "username", "password"})
    @BeforeTest
    public void customerLogin(String url, String serviceurl, String username, String password) throws IOException {
        sid = SourceBpms.crawlCustomerSid(url, serviceurl, username, password);
    }

    @Parameters({"serviceurl"})
    @Test
    public void queryBuyerTestCase(String serviceurl) throws IOException {
        HttpClientBuilder builder = HttpClientBuilder.create();
        CloseableHttpClient client = builder.build();
        HttpPost httpPostT = new HttpPost(serviceurl + "/data/bpms/buyer/buyer/archives%20");

        String postSid = ToolAggregation.subString(sid, "Set-Cookie:sid=", "; Expires=");
        System.out.println("sid的值为------>>>>>>>" + postSid);

        httpPostT.setHeader("Content-Type", "application/json;charset=UTF-8");
        httpPostT.setHeader("Cookie", "_ga=GA1.2.440190523.1513853121;" + "sid=" + postSid);

        String queryJson = "{\"condition\":{\"code\":\"UKR09160\"},\"pagingQuery\":{\"pageIndex\":1,\"pageSize\":50}}";
        StringEntity requestEntity = new StringEntity(queryJson, "utf-8");
        requestEntity.setContentEncoding("UTF-8");
        requestEntity.setContentType("application/json");
        httpPostT.setEntity(requestEntity);

        CloseableHttpResponse response = client.execute(httpPostT);
        HttpEntity entity = response.getEntity();
        String entityStr = null;
        if (entity != null) {
            entityStr = EntityUtils.toString(entity, "utf-8");
            System.out.println("输出查询结果：" + entityStr);
        }

        //转换返回json格式并获取key：success的值作为后续断言结果
        JSONObject jo = JSONObject.parseObject(entityStr);
        resultcode = jo.getBoolean("success");
        System.out.println("输出查询结果：---------------->>>>>>>>>>>>" + resultcode);

        //加断言，resultcode为作为判断的原值，expcted：false为期望值，第三个参数为说明信息，注意原值和期望值类型
        Assert.assertEquals(resultcode, true, "Not equals:");

        //数据库检查该数据是否真实存在或部分没有json返回的直接检查数据库进行对比
        Connection conn = null;
        try {
            //读取配置文件链接数据库
            QueryDataSet dataSet = ToolAggregation.connectSql(ToolAggregation.readsqlconfig()[0], ToolAggregation.readsqlconfig()[1], ToolAggregation.readsqlconfig()[2]);

            //直接在数据库根据采购商编码查询结果，并将查询的结果数据写入result文件
            dataSet.addTable("buy_buyer", "select * from buy_buyer where code ='UKR09160'");
            FlatXmlDataSet.write(dataSet, new FileOutputStream("E:\\working\\EssaTest\\src\\main\\java\\xml\\result.xml"));
            IDataSet xmlRsultDataSet = new FlatXmlDataSet(new FileInputStream("E:\\working\\EssaTest\\src\\main\\java\\xml\\result.xml"));
            ITable xmlRsultTable = xmlRsultDataSet.getTable("buy_buyer");
            xmlRsultTable = DefaultColumnFilter.includedColumnsTable(xmlRsultTable, new String[]{"id", "code", "mobile"});
            System.out.println("查询数据库结果为：采购商ID=" + xmlRsultTable.getValue(0, "id") + "采购商编号" + xmlRsultTable.getValue(0, "code"));

            //读取期望结果配置文件
            IDataSet xmlDataSet = new FlatXmlDataSet(new FileInputStream("E:\\working\\EssaTest\\src\\main\\java\\xml\\assertEquals.xml"));
            ITable xmlTable = xmlDataSet.getTable("buy_buyer");
            xmlTable = DefaultColumnFilter.includedColumnsTable(xmlTable, new String[]{"id", "code", "mobile"});

            //只对比id、code、mobile的值
            Assertion.assertEquals(xmlRsultTable, xmlTable);

        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    @AfterSuite
    public void closeClient() {
        try {
            if (client != null) {
                client.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
